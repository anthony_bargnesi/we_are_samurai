#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
 
#include <X11/Xlib.h>
#include <X11/X.h>
 
#define VERSION "0.1"
#define DEFAULT_DISPLAY ":0"
#define DEFAULT_DELAY 10000
#define BIT(c, x) ( c[x/8]&(1<<(x%8)) )
#define TRUE 1
#define FALSE 0
 
#define KEYSYM_STRLEN 64
 
#define SHIFT_DOWN 1
#define LOCK_DOWN 5
#define CONTROL_DOWN 3
#define ISO3_DOWN 4
#define MODE_DOWN 5
 
/* It is pretty standard */
#define SHIFT_INDEX 1 /*index for XKeycodeToKeySym(), for shifted keys*/
#define MODE_INDEX 2
#define MODESHIFT_INDEX 3
#define ISO3_INDEX 4
#define ISO3SHIFT_INDEX 4

/* Global variables */
extern Display *disp;
extern int PrintUp;
 
Display *disp;
int PrintUp = FALSE;
 
char *KeyCodeToStr(int code, int down, int mod);
 
int KeyModifiers(char *keys);
 
void cleaner(int sig)
{
    fflush(stdout);
    exit(0);
}
 
int main(int argc, char *argv[])
{
    char *char_ptr,
         buf1[32], buf2[32],
         *keys,
         *saved;
    int i;
     
    signal(SIGINT, cleaner);
    signal(SIGTERM, cleaner);
     
    /* setup Xwindows */
    disp = XOpenDisplay(DEFAULT_DISPLAY);
    if (disp == NULL) {
        fprintf(stderr, "Cannot open X display: %s\n", DEFAULT_DISPLAY);
        exit(1);
    }
    XSynchronize(disp, TRUE);
     
    /* setup buffers */
    saved = buf1;
    keys = buf2;
    XQueryKeymap(disp, saved);

    while (1) {
        /* find changed keys */
        XQueryKeymap(disp, keys);
        for (i = 0; i < 32*8; i++) {
            if (BIT(keys, i) != BIT(saved, i)) {
                register char *str;
                str = (char*) KeyCodeToStr(i, BIT(keys, i), KeyModifiers(keys));
                if ((BIT(keys, i) != 0 || PrintUp) && (str[0] != '\0')) {
                    pid_t pid = fork();
                    if (pid == 0) {
                        exec("play -q key.wav");
                        exit(0);
                    }
                }
            }
        }
         
        /* swap buffers */
        char_ptr = saved;
        saved = keys;
        keys = char_ptr;
         
        usleep(10000);
    }

    return 0;
}
 
/* This part takes the keycode and makes an output string. */
 
/*
 * Have a keycode, Look up keysym for it.
 * Convert keysym into its string representation.
 * if string is more than one character try to reduce it to one.
 * if string still is more than one character, put it into the form
 * (+string) or (-string) depending on whether the key is up or down.
 * print out the string.
 * */
 
struct conv {char from[20], to[5];} conv_table[] = {
    /* shift & control replaced with nothing, since they are appearent
     * from the output */
    {"return",""}, {"escape","^["}, {"delete", "^H"},
    {"shift",""}, {"control",""}, {"tab","\t"},
    {"space", " "}, {"exclam", "!"}, {"quotedbl", "\""},
    {"numbersign", "#"}, {"dollar", "$"}, {"percent", "%"},
    {"ampersand", "&"}, {"apostrophe", "'"}, {"parenleft", "("},
    {"parenright", ")"}, {"asterisk", "*"}, {"plus", "+"},
    {"comma", ","}, {"minus", "-"}, {"period", "."},
    {"slash", "/"}, {"colon", ":"}, {"semicolon", ";"},
    {"less", "<"}, {"equal", "="}, {"greater", ">"},
    {"question", "?"}, {"at", "@"}, {"bracketleft", "["},
    {"backslash", "\\"}, {"bracketright", "]"}, {"asciicircum", "^"},
    {"underscore", "_"}, {"grave", "`"}, {"braceleft", "{"},
    {"bar", "|"}, {"braceright", "}"}, {"asciitilde", "~"},
    {"",""}
};
 
int StrToChar(char *data)
{
    int i = 0;
    while (conv_table[i++].from[0] != 0 || conv_table[i++].to[0] != 0) {
        if (!strncasecmp(data, conv_table[i].from,
                    strlen(conv_table[i].from))) {
            strcpy(data, conv_table[i].to);
            return TRUE;
        }
    }
    return FALSE;
}
 
char *KeyCodeToStr(int code, int down, int mod)
{
    static char *str, buf[KEYSYM_STRLEN + 1];
    int index;
    KeySym keysym;
    /* get the keysym for the appropriate case */
    switch (mod) {
        case SHIFT_DOWN:
            index = SHIFT_INDEX;
            break;
        case ISO3_DOWN:
            index = ISO3_INDEX;
            break;
        case MODE_DOWN:
            index = MODE_INDEX;
            break;
        default:
            index = 0;
    }
     
    keysym = XKeycodeToKeysym(disp, code, index);
    if (NoSymbol == keysym) return "";
     
    /* convert keysym to a string, copy it to a local area */
    str=XKeysymToString(keysym);
     
    if (strcmp(str,"ISO_Level3_Shift") == 0) {
        keysym = XKeycodeToKeysym(disp, code, ISO3_INDEX);
        str = XKeysymToString(keysym);
    }
     
    if (str == NULL) return "";
    strncpy(buf, str, KEYSYM_STRLEN); buf[KEYSYM_STRLEN] = 0;
     
    /* try to reduce strings to character equivalents */
    if (buf[1] != 0 && !StrToChar(buf)) {
        if (strcmp(buf, "Caps_Lock") == 0) return "";
        /* still a string, so put it in form (+str) or (-str) */
        if (down) strcpy(buf, "(+");
        else strcpy(buf, "(-");
        strcat(buf, str);
        strcat(buf, ")");
        return buf;
    }
    if (buf[0] == 0) {
        return "";
    }
    if (mod == CONTROL_DOWN) {
        buf[2] = 0;
        buf[1] = toupper(buf[0]);
        buf[0] = '^';
    }
    if (mod == LOCK_DOWN) {
        buf[0] = toupper(buf[0]);
    }
    return buf;
}
 
 
/* returns which modifier is down, shift/caps or control */
int KeyModifiers(char *keys)
{
    static int setup = TRUE;
    static int width;
    static XModifierKeymap *mmap;
    int i;
     
    if (setup) {
        setup = FALSE;
        mmap = XGetModifierMapping(disp);
        width = mmap->max_keypermod;
    }
    for (i = 0; i < width; i++) {
        KeyCode code;
         
        code = mmap->modifiermap[ControlMapIndex*width+i];
        if (code && 0 != BIT(keys, code)) {return CONTROL_DOWN;}
         
        code = mmap->modifiermap[ShiftMapIndex*width +i];
        if (code && 0 != BIT(keys, code)) {return SHIFT_DOWN;}
         
        code = mmap->modifiermap[LockMapIndex*width +i];
        if (code && 0 != BIT(keys, code)) {return LOCK_DOWN;}
         
        code = mmap->modifiermap[Mod3MapIndex*width +i];
        if (code && 0 != BIT(keys, code)) {return ISO3_DOWN;}
         
        code = mmap->modifiermap[Mod5MapIndex*width +i];
        if (code && 0 != BIT(keys, code)) {return MODE_DOWN;}
    }
    return 0;
}

void parse_command(char *cmd, char **argv) {
    while (*cmd != '\0') {
        while (*cmd == ' ') *cmd++ = '\0';
        *argv++ = cmd;
        while (*cmd != '\0' && *cmd != ' ') cmd++;
    }
    *argv = '\0';
}

pid_t exec(const char *cmd) {
    char * cmd_dup = strdup(cmd);
    char *argv[64];
    parse_command(cmd_dup, argv);
    pid_t pid = fork();
    if (pid < 0) {
        perror("fork failed");
    } else if (pid == 0) {
        execvp(*argv, argv);
        exit(0);
    }
    free(cmd_dup);
    return pid;
}

